from engine import *
import time
import random

class player():

        LST_PLAYER = []

        def __init__(self, name, life, weapon, score = []):
                # -- outgame data -- #
                self.name = name
                self.score = score

                # -- ingame data -- #
                self.life = life
                self.weapon = weapon

                self.refresh_life_img = False

                player.LST_PLAYER.append(self)

        def _refresh_life(self):
                self.refresh_life_img = True


        def _gain_life(self):
                if (self.life < 10):
                        self.life += 1
                        self._refresh_life()
                        
        def _lose_life(self):
                if (self.life > 0):
                        self.life -= 1
                        self._refresh_life()
                        self._check_alive()
                
        def _check_alive(self):
                if (self.life <= 0):
                        print("aaargh")

        def _die(self, x, y):
                self._lose_life()

        def _fire(self, target, x, y):
                self.weapon._fire(target, x, y)
                


class weapon():

        LST_RECHARGING = []
        
        def __init__(self, name, magazine, magazineMax, damage, cooldown):
                self.name = name
                self.clock = clock()
                self.magazine = magazine
                self.magazineMax = magazineMax
                self.damage = damage
                self.cooldown = cooldown

                self.clock._set_trigger(self.cooldown)

        def _check_magazine(self):
                if (self.magazine !=0):
                        return True
                return False

        def _reload(self):
                if (self.clock.tstart == 0):
                        self.clock._start()
                if (self.clock._check()):
                        self.magazine = self.magazineMax
                        print("weapon recharged ! ")
                        self.clock.tstart = 0
                        weapon.LST_RECHARGING.remove(self)
                else:
                        self.clock._increment()
                        print("recharging ...")

        def _fire(self, target, x, y):
                if (self._check_magazine() and (self not in weapon.LST_RECHARGING)):
                        self.magazine -= 1
                        print(self.magazine)

                        if (target == "player"):
                                player.LST_PLAYER[0]._die(x, y)
                        else:
                                for obj in obj2D.LST:
                                         if (obj.sprite == target):
                                                 print(obj.name)
                                                 obj._die(x, y)
                else:
                        if (self not in weapon.LST_RECHARGING):
                                weapon.LST_RECHARGING.append(self)

# -- DOC --
# obj2D
# [|r| length] @sizeX
# [|r| height] @sizeY
# [|r| color]
# ---------
class obj2D():

        ID = -1
        LST = []
        LST_TO_GENERATE = []
        #LST_LAYER_FACTOR = [0, 0.477, 0.600, 0.667, 0.625, 0.600] #deprecated
        LAYER_FACTOR = 0.6

        def __init__(self, canvas, sizex, sizey, color, name = "obj2D_", anchor = "CENTER"):
                self.ID = obj2D.ID + 1
                self.name = name+str(self.ID)
                self.size = [sizex, sizey]
                self.anchor = self._set_anchor(anchor)
                self.layer = 2
                self.factor = 1
                self.color = color
                self.canvas = canvas
                self.sprite = self.canvas.create_rectangle(-1, -1, -1, -1)
                self.coords = [0, 0, self.size[0], self.size[1]]
                self.clock = clock()

                obj2D.ID += 1
                obj2D.LST.append(self)
                obj2D.LST_TO_GENERATE.append(self)


        def _set_coords(self, x, y, layer):
                if (layer >= self.layer):
                        self.coords = [x-(self.anchor[0]*self.factor), y-(self.anchor[1]*self.factor), x+(self.anchor[0]*self.factor), y+(self.anchor[1]*self.factor)]
                else:
                        self.coords = [x-(self.anchor[0]/self.factor), y-(self.anchor[1]/self.factor), x+(self.anchor[0]/self.factor), y+(self.anchor[1]/self.factor)]
                for i in range(len(self.coords)):
                        self.coords[i] = int(self.coords[i])

        def _get_coords(self):
                self.coords = self.canvas.coords(self.sprite)
                for i in range(len(self.coords)):
                        self.coords[i] = int(self.coords[i])
                
        def _set_anchor(self, anchor):
                return [(self.size[0]/2), (self.size[1]/2), anchor]

	
        def _get_layer(self, y):
                if (y <= 335):
                        return 5
                if (y <= 350):
                        return 4
                if (y <= 375):
                        return 3
                if (y <= 420):
                        return 2
                if (y <= 500):
                        return 1
                return 0


        def _set_layer_factor(self, layer):
                #return (2**(layer - self.layer)) #deprecated
                        self.factor = obj2D.LAYER_FACTOR**(abs(layer - self.layer))


        def _generate(self, x, y, layer = None, color = False):
                if (layer == None):
                        layer = self._get_layer(y)
                print(layer)
                self._set_layer_factor(layer)
                self._set_coords(x, y, layer)
                if (color):
                        self.canvas.coords(self.sprite, self.coords[0], self.coords[1], self.coords[2], self.coords[3])# = self.canvas.create_rectangle(self.coords[0], self.coords[1], self.coords[2], self.coords[3], fill = self.color)
                        self.canvas.itemconfig(self.sprite, fill = self.color)
                else:
                         self.canvas.coords(self.sprite, self.coords[0], self.coords[1], self.coords[2], self.coords[3])#self.sprite = self.canvas.create_rectangle(self.coords[0], self.coords[1], self.coords[2], self.coords[3])

                print(self.name," : ", self.coords)
                print(self.name," @ ",self.canvas.coords(self.sprite))
                for obj in obj2D.LST_TO_GENERATE:
                        if obj.ID == self.ID:
                                obj2D.LST_TO_GENERATE.remove(self)

        def _die(self, x, y):
                self.canvas.coords(self.sprite, -1, -1, -1, -1)
                self._get_coords()


# -- DOC --
# tile
# [|r| name] @gamename
# [|r| length] @sizeX
# [|r| height] @sizeY
# [|r| color]
# ---------
class tile(obj2D):

        def __init__(self, gamename, canvas, sizex, sizey, color, name = "tile_", anchor = "CENTER"):
                obj2D.__init__(self, canvas, sizex, sizey, color, name, anchor)
                
                self.gamename = gamename
                self.lst_impact = []


        def _die(self, x, y):
                self.lst_impact.append(self.canvas.create_rectangle(x-2, y-2, x+2, y+2, fill="black"))

# -- DOC --
# desperados
# [|r| name] @gamename
# [|r| length] @sizeX
# [|r| height] @sizeY
# [|r| color]
# [|r| life]
# [|r| weapon]
# [|o| tool]
# [|r| speed] +{int(1,50)}
# [|r| |f| IA] =IA
# ---------
class desperados(obj2D):

        def __init__(self, gamename, canvas, sizex, sizey, color, life, weapon, speed, IA = None, name = "desp_", anchor = "CENTER"):
                obj2D.__init__(self, canvas, sizex, sizey, color, name, anchor)

                self.gamename = gamename
                self.maxLife = life
                self.life = life
                self.weapon = weapon
                self.crouch = False
                self.speed = speed

                self.IA = IA

                #print(self.__dict__)
        
        def _setIA(self, IA):
                self.IA = IA
                self.IA._set_entity(self)

        def _startIA(self):
                self.IA.running = 1
                self.IA._run()

        def _stopIA(self):
                self.IA.running = 0

        def _crouch(self):
                if (self.crouch):
                        self.canvas.coords(self.sprite, self.coords[0], self.coords[3] - (self.coords[3]-self.coords[1])/0.625, self.coords[2], self.coords[3])
                        self.crouch = False
                else:
                        self.canvas.coords(self.sprite, self.coords[0], self.coords[3] - (self.coords[3]-self.coords[1])*0.625, self.coords[2], self.coords[3])
                        self.crouch = True
                self._get_coords()
                        
        def _move(self, vectX, vectY):
                #print(vectX, vectY)
                #print(self.canvas.coords(self.sprite))
                self.canvas.coords(self.sprite, self.coords[0]+(self.speed*vectX), self.coords[1]+(self.speed*vectY), self.coords[2]+(self.speed*vectX), self.coords[3]+(self.speed*vectY))
                self._get_coords()

        def _move_instant(self, vectX, vectY):
                self.canvas.coords(self.sprite, self.coords[0]+vectX, self.coords[1]+vectY, self.coords[2]+vectX, self.coords[3]+vectY)
                self._get_coords()

        def _fire(self, target, x=-1, y=-1):
                self.weapon._fire(target, x, y)

        def _die(self, x, y):
                self.life -= 1
                if (self.life <= 0):
                        self._stopIA()
                        self.canvas.coords(self.sprite, -1, -1, -1, -1)
                        self.canvas.coords(self.sprite)
                        self.life = self.maxLife
                        print(gameClock.CLOCK._counter())
                        print(gameClock.CLOCK.factorMin)
                        print(4 - gameClock.CLOCK._counter()/gameClock.CLOCK.factorMin)
                        self.clock._set_trigger(random.uniform(4 - gameClock.CLOCK._counter()/gameClock.CLOCK.factorMin, 8 - gameClock.CLOCK._counter()/gameClock.CLOCK.factorMax))
                        print("TRIGGER", self.clock.trigger)
                        obj2D.LST_TO_GENERATE.append(self)
                        self.clock._start()
                        



if __name__ == "__main__":
        print("ok")
