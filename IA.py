import engine

class IA():

	LST_RUNNING = []

	def __init__(self, entity, nb_state):
		self.entity = entity
		self.clock = engine.clock()

		self.state = [0 for k in range(nb_state)]
		self.running = 0

		self._reset()

	def _set_entity(self, entity):
		self.entity = entity

	def _reset(self):
		for i in range(len(self.state)):
			self.state[i] = 0
		self.state[0] = 1

	def _next(self):
		for i in range(len(self.state)):
			if (self.state[i]):
				self.state[i], self.state[i+1] = self.state[i+1], self.state[i]
				return None
	
	def _stop(self):
		try:
			IA.LST_RUNNING.remove(self)
			self._reset()
		except ValueError:
			pass

# ----------------------------- IATRANSLATE ---------------------------------------------------------------------

# ---------------------------------------------------------------------------------------------------------------
class IATranslate(IA):

	def __init__(self, t1, t2, t3, vectX, vectY, xlim, ylim, nb_state = 10, entity = None):
		IA.__init__(self, entity, nb_state)

		self.t1 = t1
		self.t2 = t2
		self.t3 = t3
		self.vect = [vectX, vectY]
		self.rellim = [(xlim*vectX), (ylim*vectY)]
		self.abslim = [0, 0]


	def _set_entity(self, entity):
		self.entity = entity

	def _run(self):
		#print(self.state)
		if not (self.running):
			self._stop()
			return None

		if (self.running) and (self not in IA.LST_RUNNING):
			IA.LST_RUNNING.append(self)

		if (self.state[0]):
			self.clock._set_trigger(self.t1)
			self.abslim = [self.entity.coords[0] + self.rellim[0], self.entity.coords[1] + self.rellim[1]]
			self.clock._start()
			self._next()

		if (self.state[1]):
			if (self.clock._check()):
				self._next()
			else:
				self.clock._increment()
		
		if (self.state[2]):
			#print(self.abslim)
			#print(self.entity.coords)
			if (self.entity.coords[0] == self.abslim[0]) and (self.entity.coords[1] == self.abslim[1]):
				self._next()
			else:
				self.entity._move(self.vect[0], self.vect[1])

		if (self.state[3]):
			self.clock._set_trigger(self.t2)
			self.clock._start()
			self._next()

		if (self.state[4]):
			if (self.clock._check()):
				self._next()
			else:
				self.clock._increment()

		if (self.state[5]):
			self.entity._fire("player")
			self._next()

		if (self.state[6]):
			self.clock._set_trigger(self.t3)
			self.abslim = [self.entity.coords[0] - self.rellim[0], self.entity.coords[1] - self.rellim[1]]
			self.clock._start()			
			self._next()

		if (self.state[7]):
			if (self.clock._check()):
				self._next()
			else:
				self.clock._increment()

		if (self.state[8]):
			#print(self.abslim)
			if (self.entity.coords[0] == self.abslim[0]) and (self.entity.coords[1] == self.abslim[1]):
				self._next()
			else:
				self.entity._move(self.vect[0]*(-1), self.vect[1]*(-1))
		
		if (self.state[9]):
			self._reset()



# ----------------------------- IATRANSLATE (INSTANT) ----------------------------------------------------------

# --------------------------------------------------------------------------------------------------------------
class IATranslateInstant(IA):

	def __init__(self, t1, t2, t3, vectX, vectY, xlim, ylim, nb_state = 10, entity = None):
		IA.__init__(self, entity, nb_state)

		self.t1 = t1
		self.t2 = t2
		self.t3 = t3
		self.vect = [vectX, vectY]
		self.rellim = [(xlim*vectX), (ylim*vectY)]
		self.abslim = [0, 0]


	def _set_entity(self, entity):
		self.entity = entity

	def _run(self):
		#print(self.state)
		if (self.state[0]):
			IA.LST_RUNNING.append(self)
			self.clock._set_trigger(self.t1)
			self.abslim = [self.entity.coords[0] + self.rellim[0], self.entity.coords[1] + self.rellim[1]]
			self.clock._start()
			self._next()

		if (self.state[1]):
			if (self.clock._check()):
				self._next()
			else:
				self.clock._increment()
		
		if (self.state[2]):
			if (self.entity.coords[0] == self.abslim[0]) and (self.entity.coords[1] == self.abslim[1]):
				self._next()
			else:
				self.entity._move_instant(self.rellim[0], self.rellim[1])

		if (self.state[3]):
			self.clock._set_trigger(self.t2)
			self.clock._start()
			self._next()

		if (self.state[4]):
			if (self.clock._check()):
				self._next()
			else:
				self.clock._increment()

		if (self.state[5]):
			self.entity._fire()
			self._next()

		if (self.state[6]):
			self.clock._set_trigger(self.t3)
			self.abslim = [self.entity.coords[0] - self.rellim[0], self.entity.coords[1] - self.rellim[1]]
			self.clock._start()			
			self._next()

		if (self.state[7]):
			if (self.clock._check()):
				self._next()
			else:
				self.clock._increment()

		if (self.state[8]):
			if (self.entity.coords[0] == self.abslim[0]) and (self.entity.coords[1] == self.abslim[1]):
				self._next()
			else:
				self.entity._move_instant(self.rellim[0]*(-1), self.rellim[1]*(-1))
		
		if (self.state[9]):
			self._reset()

# ----------------------------- IACROUCH -----------------------------------------------------------------------

# --------------------------------------------------------------------------------------------------------------
class IACrouch(IA):

	def __init__(self, t1, t2, t3, nb_state = 10, entity = None):
		IA.__init__(self, entity, nb_state)

		self.t1 = t1
		self.t2 = t2
		self.t3 = t3


	def _set_entity(self, entity):
		self.entity = entity
		self.entity._crouch()

	def _run(self):
		#print(self.state)
		if (self.state[0]):
			IA.LST_RUNNING.append(self)
			self.clock._set_trigger(self.t1)
			self.clock._start()
			self._next()

		if (self.state[1]):
			if (self.clock._check()):
				self._next()
			else:
				self.clock._increment()
		
		if (self.state[2]):
			if (self.entity.crouch):
                                self.entity._crouch()
			else:
				self._next()

		if (self.state[3]):
			self.clock._set_trigger(self.t2)
			self.clock._start()
			self._next()

		if (self.state[4]):
			if (self.clock._check()):
				self._next()
			else:
				self.clock._increment()

		if (self.state[5]):
                        if (self.entity.weapon._check_magazine()):
                                self.entity._fire()
                        else:
                                self._next()

		if (self.state[6]):
			self.clock._set_trigger(self.t3)
			self.clock._start()			
			self._next()

		if (self.state[7]):
			if (self.clock._check()):
				self._next()
			else:
				self.clock._increment()

		if (self.state[8]):
			if (self.entity.crouch):
                                self._next()
			else:
				self.entity._crouch()
		
		if (self.state[9]):
                        if (self.entity.weapon.magazine == self.entity.weapon.magazineMax):
                                self._reset()
                        else:
                                self.entity.weapon._fire()
