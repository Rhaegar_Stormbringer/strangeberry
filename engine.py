import time
import tkinter
import entities
import IA
import random

class clock():

	def __init__(self):
		self.trigger = 0
		self.tstart = 0
		self.tick = 0

	def _set_trigger(self, x):
		self.trigger = x

	def _start(self):
		self.tstart = time.time()

	def _increment(self):
		self.tick = time.time()

	def _check(self):
		return (self.tick - self.tstart > self.trigger)


class gameClock(clock):

	CLOCK = None

	def __init__(self, T, counterLimit, factorMin, factorMax):
		clock.__init__(self)
		self._set_trigger(T)
		self.counterLimit = counterLimit
		self.factorMin = factorMin
		self.factorMax = factorMax
		gameClock.CLOCK = self
		

	def _counter(self):
		return (self.tick - self.tstart)

if __name__ == "__main__":
	print("ok")
