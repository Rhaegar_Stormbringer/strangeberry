from tkinter import *
from entities import *
from engine import *
from IA import *


import time
import random
#from PIL import Image, ImageTk

GUI_WIDTH = 800
GUI_HEIGHT = 600

class game():

        LST_GAMEPHASE = []
        ACTUAL_GAMEPHASE = None

        def __init__(self):
                self.pause = 0

class BattleCounter():

        def __init__(self, T, counterLimit, respawnTimeMin, respawnTimeMax):
                self.factor = [counterLimit/respawnTimeMin, counterLimit/respawnTimeMax]
                print(self.factor)
                self.gameClock = gameClock(T, counterLimit, self.factor[0], self.factor[1])

test = BattleCounter(120, 90, 3, 6)

def gameGenerateEntities():
        for obj in obj2D.LST_TO_GENERATE:
                if (obj.clock._check()):
                        obj._generate(400, 500, color="True")
                        obj._startIA()
                else:
                        obj.clock._increment()

def mouse2(event):
        p._lose_life()
        #r = random.randint(0, 1)
        #if (r):
                #p._gain_life()
        #else:
                #p._lose_life()
                
	
def mouse1(event):
    co = [event.x-5, event.y-5, event.x+5, event.y+5]
    #cav.create_rectangle(p[0], p[1], p[2], p[3], fill = "black")
    try:
        i = cav.find_overlapping(co[0], co[1], co[2], co[3])[-1]
        p._fire(i, event.x, event.y)
        #print("i = ",i)
        #for obj in entities.obj2D.LST:
        #print(obj.sprite)
           # if (obj.sprite == i):
                #print(obj.name)
                #obj._die(event.x, event.y)
    except IndexError:
        return None
                
gui = Tk()
cav = Canvas(gui, width = GUI_WIDTH, height = GUI_HEIGHT, bg = 'white')
cav.pack()

#fond = Image.open("sprites/fond.png")
#fond = ImageTk.PhotoImage(fond)
#cav.create_image(0, 0, anchor=NW, image=fond)

cav.create_rectangle(0, GUI_HEIGHT-100, GUI_WIDTH, GUI_HEIGHT-100, fill = "purple")
cav.create_rectangle(0, GUI_HEIGHT-180, GUI_WIDTH, GUI_HEIGHT-180, fill = "purple")
cav.create_rectangle(0, GUI_HEIGHT-225, GUI_WIDTH, GUI_HEIGHT-225, fill = "purple")
cav.create_rectangle(0, GUI_HEIGHT-250, GUI_WIDTH, GUI_HEIGHT-250, fill = "purple")
cav.create_rectangle(0, GUI_HEIGHT-265, GUI_WIDTH, GUI_HEIGHT-265, fill = "purple")

w = weapon("The Windowmaker", 6, 6, 12, 4)
p = player("Rhaegar", 10, w, None)

d = desperados("Bob", cav, 100, 100, "red", 3, w, 1)
d._generate(400, 500, color=True)

#b = tile("barrel", cav, 100, 100, "yellow")
#b._generate(350, 500, color=True)

i = IATranslate(6, 2, 2, -1, 0, 50, 0)
d._setIA(i)

#f = desperados("Frank", cav, 50, 80, "red", 3, w, None, 3)
#f._generate(500, 400, 1)

#o = obj2D(cav, 100, 100, "brown")
#o._generate(400, 300, 1)

#p = obj2D(cav, 100, 100, "brown")
#p._generate(400, 300, 5)

#q = obj2D(cav, 100, 100, "brown")
#q._generate(100, 100, 2, True)

lst_life_img = [cav.create_rectangle(200, 550, 600, 580, fill="grey")]
for i in range(10):
        lst_life_img.append(cav.create_rectangle(200+(41*i), 550, 230+(41*i), 580, fill="green"))

gui.bind("<Button-1>", mouse1)
gui.bind("<Button-3>", mouse2)

T = time.time()

d._startIA()

test.gameClock._start()
while(1):
    #print(weapon.LST_RECHARGING)
    #print(obj2D.LST_TO_GENERATE)
    for weapon in weapon.LST_RECHARGING:
        weapon._reload()

    for ia in IA.LST_RUNNING:
        ia._run()
    #print(IA.LST_RUNNING)
    gameGenerateEntities()

    test.gameClock._increment()
    #print(d.life)
    #print(IA.LST_RUNNING)
    #d.IA._run()
    t = time.time()
    if (p.refresh_life_img):
            print(p.life)
            try:
                    for i in range(p.life):
                            cav.itemconfig(lst_life_img[i+1], fill="green")
                    for i in range(p.life+1, len(lst_life_img)):
                            cav.itemconfig(lst_life_img[i], fill="red")
            except IndexError:
                    pass
            p.refresh_life_img = False
    #print(d.IA.clock.tstart)
    #print("{0:.2f}".format((t - T)))
    #sleep(0.1)
    cav.update()

gui.mainloop()
